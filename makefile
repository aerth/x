PREFIX ?= ${HOME}/.local/bin
all:
	go get -v -d ./...
	gofmt -w -l -s .
	@echo running interface check
	go run test.go
	@echo done running sanity check
	@echo building...
	go run build.go
	@echo done building

install:
	install -v ${PWD}/bin/* ${PREFIX}

clean:
	rm -rv bin


