// +build ignore

package main

import (
	"os"

	"gitlab.com/aerth/x"
	"gitlab.com/aerth/x/encoding/basex"
	"gitlab.com/aerth/x/encoding/rlp"
	"gitlab.com/aerth/x/mirror/color"
)

func main() {
	s := rlp.Serializer{}
	se := basex.NewAlphabet(basex.Base58Bitcoin)
	tester(s, se)
	tester2(s, se)
}

func tester(serializer x.Serializer, stringEnc x.StringEncoder) {
	thing := struct {
		I uint8
		N uint64
		B []byte
		S string
	}{10, 0xF43994, []byte("hello"), "world"}
	b, err := serializer.Serialize(thing)
	if err != nil {
		panic(err)
	}
	if len(b) == 0 {
		panic("zero")
	}
	color.Blue("begin:      %v\n", thing)
	color.Blue("serialized: %x\n", b)
	color.Unset()
	s := stringEnc.Encode(b)
	color.Green("encoded:    %s\n", s)
}

func tester2(serializer x.Serializer, stringEnc x.StringEncoder) {
	enc := "9ftELCZsB8vyskLfWQHgxro9d"
	thing := struct {
		I uint8
		N uint64
		B []byte
		S string
	}{}
	b, err := stringEnc.Decode(enc)
	if err != nil {
		red := color.New(color.FgRed).PrintfFunc()
		red("Fatal: %v", err)
		os.Exit(111)
	}
	if err := serializer.Deserialize(b, &thing); err != nil {
		red := color.New(color.FgRed).PrintfFunc()
		red("Fatal: %v", err)
		os.Exit(111)
	}
	color.Green("decoded:    %v\n", thing)
}
