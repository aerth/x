// Copyright (c) 2018 aerth. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of aerth nor the names of this project's
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package gotchas

import (
	"fmt"
	"net"
	"sort"
)

func ExampleParseIPWithPort() {
	// with port (invalid)
	ip := net.ParseIP("127.0.0.1:1337")
	ipv6 := net.ParseIP("2001:db8::68:1337")
	fmt.Println(ip)
	fmt.Println(ipv6)

	// without port (valid)
	ip = net.ParseIP("127.0.0.1")
	ipv6 = net.ParseIP("2001:db8::68")
	fmt.Println(ip)
	fmt.Println(ipv6)
	// Output:
	// <nil>
	// 2001:db8::68:1337
	// 127.0.0.1
	// 2001:db8::68
}

func ExampleConcurrency() {
	var (
		lim     = 4
		numchan = make(chan int)
		numbers = make([]int, lim)
	)
	for i := 0; i < lim; i++ {
		// i := i // uncomment this line ;)
		go func() {
			numchan <- i
		}()
	}
	for i := 0; i < lim; i++ {
		in := <-numchan
		numbers[i] = in
	}

	sort.Ints(numbers)
	fmt.Println(numbers)

	// Output: [4 4 4 4]
}
