// not even real
package sizes

var (
	Bit  = 1
	Byte = 8
	B    = Byte
	KB   = 1024 * B
	MB   = 1024 * KB
	GB   = 1024 * MB
	TB   = 1024 * GB
	PB   = 1024 * TB
	EB   = 1024 * PB
)
