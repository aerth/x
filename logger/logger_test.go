// Copyright (c) 2018 aerth. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of aerth nor the names of this project's
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package logger

import (
	"os"
	"sync"
	"testing"
	"time"
)

func TestOne(t *testing.T) {
	l := New(os.Stderr, "", 0)
	l.Info("hello, world")
	l.Debug("it works")

	rpclog := New(os.Stderr, "[RPC] ", 0)
	rpclog.Info("hello, world", 123, uint64(1e13))
	rpclog.Debug("it works")
	g := New(os.Stderr, "[concurrent] ", 0)
	wg := new(sync.WaitGroup)
	workers := 10
	for i := 0; i < workers; i++ {
		go func(n int) {
			wg.Add(1)
			g.Infof("cool! %x %s", n, time.Now())
			wg.Done()
		}(i)
	}
	finished := make(chan struct{})
	go func() {
		wg.Wait()
		finished <- struct{}{}
	}()
	select {
	case <-finished:
		l.Info("finished")
	case <-time.After(time.Second * 30):
		l.Error("timeout")
	}

}
