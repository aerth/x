// Copyright (c) 2018 aerth. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of aerth nor the names of this project's
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package logger

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
	"time"
)

type Logger struct {
	out    io.Writer
	prefix string
	flag   int
	level  int
}

const (
	Info  = 0
	Debug = 1
	Trace = 2
)

func New(out io.Writer, prefix string, flag int) *Logger {
	if out == nil {
		out = os.Stderr
	}
	return &Logger{out: out, prefix: prefix, flag: flag}
}

func (l *Logger) SetLevel(n int) {
	l.level = n
}
func (l *Logger) SetFlags(n int) {
	l.flag = n
}
func (l *Logger) Error(v ...interface{}) {
	l.write("ERROR", v...)
}
func (l *Logger) Errorf(f string, v ...interface{}) {
	l.writef("ERROR", f, v...)
}
func (l *Logger) Warn(v ...interface{}) {
	l.write("WARN", v...)
}
func (l *Logger) Warnf(f string, v ...interface{}) {
	l.writef("WARN", f, v...)
}
func (l *Logger) Debug(v ...interface{}) {
	if l.level < Debug {
		return
	}
	l.write("DEBUG", v...)
}
func (l *Logger) Debugf(f string, v ...interface{}) {
	if l.level < Debug {
		return
	}
	l.writef("DEBUG", f, v...)
}
func (l *Logger) Trace(v ...interface{}) {
	if l.level < Trace {
		return
	}
	l.write("TRACE", v...)
}
func (l *Logger) Tracef(f string, v ...interface{}) {
	if l.level < Trace {
		return
	}

	l.writef("TRACE", f, v...)
}
func (l *Logger) Info(v ...interface{}) {
	l.write("INFO", v...)
}
func (l *Logger) Infof(f string, v ...interface{}) {
	l.writef("INFO", f, v...)
}

func spacer(s ...string) string {
	var ss []string
	for i := range s {
		if s[i] == "" {
			continue
		}
		ss = append(ss, s[i])
	}
	if len(ss) == 0 {
		return ""
	}
	return strings.Join(s, " ")
}

func (l *Logger) pre() string {
	switch l.flag {
	case 0:
		return ""
	case 1:
		return time.Now().Format(time.StampMilli)
	case 2:
		return getcaller()
	}
	return ""
}

func getcaller() string {
	calldepth := 4
	var short string
	_, file, line, ok := runtime.Caller(calldepth)

	if !ok {

		file = "???"

		line = 0

	} else {

		for i := len(file) - 1; i > 0; i-- {

			if file[i] == '/' {

				short = file[i+1:]

				break

			}

		}
		file = short
	}

	return fmt.Sprintf("%s:%v", file, line)

}

func (l *Logger) writef(level string, f string, v ...interface{}) {

	fmt.Fprintf(l.out, spacer(l.pre(), level, l.prefix)+": "+f+"\n", v...)
}
func (l *Logger) write(level string, v ...interface{}) {
	fmt.Fprintf(l.out, "%s: ", spacer(l.pre(), level, l.prefix))
	fmt.Fprintln(l.out, v...)
}
func (l *Logger) Fatal(v ...interface{}) {
	l.write("FATAL:", v...)
	os.Exit(111)
}
