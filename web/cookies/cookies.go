package cookies

import "net/http"

func NewCookie(key, value string) *http.Cookie {
	return &http.Cookie{Name: key, Value: value}
}
