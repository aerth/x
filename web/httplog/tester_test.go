package httplog

import (
	"bytes"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestLog(t *testing.T) {
	buf := &bytes.Buffer{}
	Logger().SetOutput(buf)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Log(r)
	}))
	defer ts.Close()

	_, err := http.Get(ts.URL)
	if err != nil {
		log.Fatal(err)
	}
	if !strings.HasPrefix(buf.String(), `GET / (0 bytes) from 127.0.0.1:`) {
		log.Println("does not match:", buf.String())
		t.FailNow()
	}

}
