package httplog

import (
	"log"
	"net/http"
	"os"
)

var logger = log.New(os.Stderr, "", 0)

func Logger() *log.Logger {
	return logger
}

func Log(r *http.Request) {
	r.ParseForm()
	logger.Printf(`%s %s (%v bytes) from %s %s`, r.Method, r.URL.Path, r.ContentLength, r.RemoteAddr, r.Form.Encode())
}

func L(r *http.Request) {
	r.ParseForm()
	logger.Printf(`%s %s (%v bytes) from %s [%q] %s`, r.Method, r.URL.Path, r.ContentLength, r.RemoteAddr, r.UserAgent(), r.Form.Encode())
}
