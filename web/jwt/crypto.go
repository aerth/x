package jwt

/* cryptographic functions */

import (
	"crypto/elliptic"
	"encoding/hex"
	"errors"
	"io"
	"io/ioutil"
	"os"

	"github.com/btcsuite/btcd/btcec"
)

// S256 returns an instance of the secp256k1 curve.
func S256() elliptic.Curve {
	return btcec.S256()
}

// NewKey generates a new *btcec.PrivateKey
func NewKey() (*btcec.PrivateKey, error) {
	return btcec.NewPrivateKey(S256())
}

// BytesToECDSA returns *btcec.PrivateKey from b
func BytesToECDSA(b []byte) (*btcec.PrivateKey, error) {
	key, _ := btcec.PrivKeyFromBytes(S256(), b)
	return key, nil

}

// FromECDSA serializes private key to bytes
func FromECDSA(key *btcec.PrivateKey) ([]byte, error) {
	return key.Serialize(), nil
}

// HexToECDSA parses a hex encoded private key
func HexToECDSA(hexkey string) (*btcec.PrivateKey, error) {
	b, err := hex.DecodeString(hexkey)
	if err != nil {
		return nil, errors.New("invalid hex string")
	}
	return BytesToECDSA(b)
}

// SaveKey writes key to file, hex encoded.
func SaveKey(key *btcec.PrivateKey, file string) error {
	return ioutil.WriteFile(file, []byte(hex.EncodeToString(key.Serialize())), 0600)
}

// LoadECDSA loads a secp256k1 private key from the given file made with SaveKey
func LoadECDSA(file string) (*btcec.PrivateKey, error) {
	buf := make([]byte, 64)
	fd, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer fd.Close()
	if _, err := io.ReadFull(fd, buf); err != nil {
		return nil, err
	}

	key, err := hex.DecodeString(string(buf))
	if err != nil {
		return nil, err
	}
	return BytesToECDSA(key)
}
