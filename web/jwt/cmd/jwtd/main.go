package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	// for saving user:hash
	// for hashing password
	"gitlab.com/aerth/x/web/jwt"
	// simple web log
)

var (
	boottime   = time.Now().UTC()
	version    string
	dateformat = `Mon Jan 2 15:04:05 MST 2006`
)

func help() {
	fmt.Fprintf(os.Stderr, "Available Commands: addperm, removeperm, serve\n")
}

var logo = `
   _)           |       | 
    |\ \  \   / __|  _` + "`" + ` | 
    | \ \  \ /  |   (   | 
    |  \_/\_/  \__|\__,_| 
___/                      
`

func main() {
	fmt.Println(logo)
	fmt.Println(version)
	log.SetFlags(log.Ltime | log.Lshortfile)
	var (
		addr      = flag.String("addr", "127.0.0.1:8080", "address:port for listen")
		dbpath    = flag.String("db", "tokens.db", "path to BoltDB database")
		keypath   = flag.String("key", "private.key", "path to private key bytes, or - for stdin (use with gpg)")
		debugflag = flag.Bool("v", false, "verbose")
		checkIP   = flag.Bool("ip", true, "check visitor ip against token")
		checkUA   = flag.Bool("ua", true, "check visitor useragent against token")
	)

	flag.Parse()

	config := &jwt.Config{
		Debug:   *debugflag,
		CheckIP: *checkIP,
		CheckUA: *checkUA,
	}

	args := flag.Args()

	if len(args) == 0 {
		flag.Usage()
		help()
		exe, err := os.Executable()
		if err == nil {
			fmt.Fprintf(os.Stderr, "\ndid you mean: '%s serve'?\n", filepath.Base(exe))
		}
		os.Exit(111)
	}

	switch args[0] {
	case "help":
		flag.Usage()
		help()
		return
	case "keygen":
		key, err := jwt.NewKey()
		if err != nil {
			log.Fatalln(err)
		}
		if err := jwt.SaveKey(key, *keypath); err != nil {
			log.Fatalln(err)
		}
		fmt.Printf("made key: %x\n\n", key.PublicKey.X)
		return
	}
	if _, err := os.Open(*keypath); err != nil {
		log.Fatalf("Could not find %q.\ntry \"keygen\" command\ngot error: %v", *keypath, err)

	}
	sys, err := jwt.NewSystem(*addr, *dbpath, *keypath, config)
	if err != nil {
		log.Fatalln("Error in setup:", err)

	}
	switch args[0] {
	// some admin stuff
	case "pubkey":
		fmt.Fprintf(os.Stdout, "%s", sys.Pubkey())
		return
	case "addperm":
		if len(args) != 3 {
			log.Println("need 3 args, got", len(args))
			return
		}
		if err := sys.AddPerm(args[1], args[2]); err != nil {
			log.Fatalln(err)
		}
		log.Println("added permission")
		return
	case "removeperm":
		if len(args) != 3 {
			log.Println("need 3 args, got", len(args))
			return
		}
		if err := sys.RemovePerm(args[1], args[2]); err != nil {
			log.Fatalln(err)
		}
		log.Println("removed permission")
		return
	case "serve":
		log.Fatalln(sys.Serve())
	default:
		log.Fatalln("unknown command:", args[0])
	}
	panic("ouch")
}
