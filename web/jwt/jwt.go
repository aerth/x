package jwt

/* javascript web token system */

import (
	"bytes"
	"crypto/ecdsa"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/kr/pretty"

	"github.com/btcsuite/btcd/btcec"
	jwt "github.com/dgrijalva/jwt-go"  // for jwt
	"gitlab.com/aerth/x/database"      // for saving user:hash
	"gitlab.com/aerth/x/hash/argon2id" // for hashing password
)

// TODO:
//
// add salt to pw db
// revisit private key

type System struct {
	db     *database.DB
	server *http.Server
	hasher func(b []byte) []byte
	key    *ecdsa.PrivateKey
	Config *Config
}

type Permissions map[string]bool

func (s *System) Pubkey() ecdsa.PublicKey {
	return s.key.PublicKey
}

var (
	ErrECDSAVerification = jwt.ErrECDSAVerification
)

func NewSystem(addr, dbpath, keypath string, config *Config) (*System, error) {
	db, err := database.Open(dbpath)
	if err != nil {
		return nil, err
	}
	var keybytes []byte
	var key *btcec.PrivateKey
	// read key (once)
	if keypath == "-" {
		buf := new(bytes.Buffer)
		io.Copy(buf, os.Stdin)
		keybytes = buf.Bytes()
		if len(keybytes) == 0 {
			log.Fatalln("could not read key from stdin")
		}
		if len(keybytes) == 0 {
			return nil, errors.New("zero key")
		}
		keybytes, err = hex.DecodeString(string(keybytes))
		if err != nil {
			return nil, err
		}
		key, err = BytesToECDSA(keybytes)
		if err != nil {
			fmt.Println("lenkey:", len(keybytes))
		}
		log.Println("OK")
	} else {
		log.Println("reading key:", keypath)
		key, err = LoadECDSA(keypath)
		if err != nil {
			return nil, err
		}
		log.Println("loaded key")
	}

	sys := &System{
		db:     db,
		hasher: argon2id.NewDefault().Sum,
		server: &http.Server{Addr: addr},
		Config: config,
		key:    key.ToECDSA(),
	}

	// set web handler
	sys.server.Handler = sys

	log.Println("signing and verifying test token")
	token := NewToken(TokenFormat{Username: "test"})
	str, err := token.SignedString(sys.key)
	if err != nil {
		return nil, err
	}
	if _, err := sys.tokenparser(str); err != nil {
		return nil, err
	}

	return sys, nil
}

func (s *System) Serve() error {
	log.Println("Launching http server")
	if s.server == nil || s.server.Addr == "" {
		return errors.New("server not configged")
	}
	// print version and listener
	go func() {
		time.Sleep(time.Second)
		fmt.Println(version)
		fmt.Printf("Serving: http://%s\n", s.server.Addr)
	}()
	// serve http
	return s.server.ListenAndServe()
}

type Config struct {
	Debug, CheckIP, CheckUA bool
}

type TokenFormat struct {
	Username  string
	Time      time.Time // issued
	Expires   time.Time // if set, overrides DefaultExpired per token
	UserAgent string
	IP        string
	UserInfo  UserInfo
	jwt.StandardClaims
}

func NewToken(token TokenFormat) *jwt.Token {
	return jwt.NewWithClaims(jwt.SigningMethodES256, token)
}

type UserInfo struct {
	Permissions Permissions
}

var (
	boottime   = time.Now().UTC()
	version    string
	hits       int64
	dateformat = `Mon Jan 2 15:04:05 MST 2006`
)

var (
	DefaultExpired  = time.Minute * 10
	ErrNoToken      = errors.New("no token")
	ErrTokenExpired = errors.New("expired token, yo")
)

func NewPermissions(perm ...string) Permissions {
	m := make(Permissions)
	for _, v := range perm {
		m[v] = true
	}
	return m
}

func (s *System) validateToken(token *jwt.Token, claims *TokenFormat) bool {
	var (
		expires time.Time
	)
	if claims.Expires.IsZero() {
		expires = claims.Time.Add(DefaultExpired)
	} else {
		expires = claims.Expires
	}
	if s.Config.Debug {
		log.Printf("parsed token, user='%s' since='%s'", claims.Username, claims.Time)
	}
	// check expired
	if time.Since(expires) > 0 {
		log.Printf("expired token, user='%s' expiretime='%s'", claims.Username, expires)
		return false
	}

	pretty.Println(token)
	pretty.Println(claims)
	// display page
	return true
}

// used for some func somewhere
func (s *System) getkey(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}
	return s.key.Public(), nil
}

// ParseTokenForm from a form["token"], parse the form before calling
func (s *System) ParseTokenForm(r *http.Request) (*jwt.Token, *TokenFormat, error) {
	// fetch token
	tokenString := r.FormValue("token")
	if tokenString == "" {
		return nil, nil, ErrNoToken
	}
	return s.ParseTokenString(tokenString)
}

// ParseTokenString from a string
func (s *System) ParseTokenString(tokenString string) (*jwt.Token, *TokenFormat, error) {
	token, err := s.tokenparser(tokenString)
	if err != nil {
		return token, nil, err
	}

	claims, ok := token.Claims.(*TokenFormat)
	if !ok || !token.Valid {
		return token, nil, errors.New("invalid token claims")
	}
	return token, claims, nil
}

// actual parsing
func (s *System) tokenparser(tokenString string) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(tokenString, &TokenFormat{}, s.getkey)
	return token, err
}
