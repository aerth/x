package jwt

/* some web handlers */

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	// for jwt
	// for saving user:hash
	// for hashing password
	"gitlab.com/aerth/x/web/httplog" // simple web log
)

// ServeHTTP is the main entrypoint / router, passing GET requests to s.nextStepGET(w,r)
func (s *System) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	httplog.Log(r)
	switch r.Method {
	default:
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	case http.MethodGet:
		s.nextStepGET(w, r)
		return
	}
}

func (s *System) nextStepGET(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	default:
		http.NotFound(w, r)
		return
	case "/new":
		name, pass, ok := getformNamePass(r)
		if !ok {
			http.Error(w, "no auth", 500)
			return
		}

		if s.checkuserExists(name) {
			log.Println(name, "tried signing up again")
			http.Error(w, "user exists", 500)
			return
		}
		if err := s.updateUser(name, pass); err != nil {
			log.Println(err)
			http.Error(w, "DB 1", 500)
			return
		}
		if err := s.updateUserInfo(name, UserInfo{Permissions: NewPermissions("guest", "view")}); err != nil {
			log.Println(err)
			http.Error(w, "DB 2", 500)
			return
		}
		log.Println("New User!", name)
		fmt.Fprintf(w, "New User\n")
		return
	case "/login":
		name, pass, ok := getformNamePass(r)
		if !ok {
			http.Error(w, "no auth", 500)
			return
		}
		if !s.checkuserpass(name, pass) {
			http.Error(w, "invalid credentials", 403)
			return
		}

		userinfo, err := s.readUserInfo(name)
		if err != nil {

			log.Println(err)
			http.Error(w, "user disabled", 500)
			return
		}
		token := NewToken(TokenFormat{
			Username:  name,
			Time:      time.Now().UTC(),
			Expires:   time.Now().UTC().Add(time.Minute * 30),
			UserAgent: r.UserAgent(),
			UserInfo:  userinfo,
			IP:        getip(r.RemoteAddr),
		})

		tokenString, err := token.SignedString(s.key)
		if err != nil {
			http.Error(w, "invalid credential signer", 403)
			return
		}
		log.Printf("redirecting to /stats?token=%s", tokenString)
		http.Redirect(w, r, "/stats?token="+tokenString, http.StatusFound)
	case "/":
		_, err := s.AuthenticateSession(r)
		if err == nil {
			http.Redirect(w, r, "/stats?token="+r.FormValue("token"), http.StatusFound)
			return
		}
		f, err := os.Open("pub/index.html")
		if err != nil {
			log.Printf("error opening index.html: %v", err)
			http.Error(w, "error", 500)
			return
		}
		defer f.Close()
		io.Copy(w, f)
		return
	case "/stats":
		// authenticated page. needs token.
		token, err := s.AuthenticateSession(r)
		// err is able to be published only if named errors
		if err != nil {
			log.Println("user not authed", err)
			switch err {
			default:
				http.Error(w, "not authenticated", http.StatusForbidden)
				return
			case ErrTokenExpired:
				http.Error(w, err.Error(), http.StatusForbidden)
				return
			}
		}
		if !token.UserInfo.Permissions["view"] {
			log.Printf("user %q does not have view permission: permissions=%s", token.Username, token.UserInfo.Permissions)
			http.Error(w, "permission denied", http.StatusForbidden)
			return
		}

		var expiresIn time.Duration
		if !token.Expires.IsZero() {
			expiresIn = time.Until(token.Expires)
		} else {
			expiresIn = time.Until(token.Time.Add(DefaultExpired))
		}
		log.Printf("welcome %s, expires in %s", token.Username, expiresIn)

		// the page
		hits++
		fmt.Fprintf(w, "Time: %s\nUptime: %s\nHits:%v\nSession Expires: %s\n",
			time.Now().UTC().Format(dateformat),
			time.Since(boottime).Round(time.Second),
			hits,
			expiresIn,
		)

	}
}

// AuthenticateSession returns error if session can not be verified
func (s *System) AuthenticateSession(r *http.Request) (*TokenFormat, error) {
	token, claims, err := s.ParseTokenForm(r)
	if err != nil {
		log.Println("quick")
		return nil, err
	}
	if !s.validateToken(token, claims) {
		return nil, ErrTokenExpired
	}

	if s.Config.CheckUA && claims.UserAgent != r.UserAgent() {
		return nil, errors.New("user agent missing")
	}

	if s.Config.CheckIP && claims.IP != getip(r.RemoteAddr) {
		return nil, errors.New("IP missing")
	}
	return claims, nil
}

// to fetch provided username and password from a http form, all or nothing
func getformNamePass(r *http.Request) (name, pass string, ok bool) {
	name = r.FormValue("name")
	pass = r.FormValue("pass")
	if name == "" || pass == "" {
		return "", "", false
	}
	return name, pass, true
}

func getip(s string) string {
	return strings.Split(s, ":")[0]
}
