// package basex implements a human-friendly base-N encoding.
package basex

import (
	"math/big"
	"strconv"

	"gitlab.com/aerth/x"
)

var _ x.StringEncoder = &Alphabet{} // Alphabet is a x.StringEncoder

const Base58Default = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"
const Base58Bitcoin = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
const Base58Flickr = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"

func NewAlphabet(s string) *Alphabet {
	decodeMap := [256]byte{}
	for i := 0; i < len(decodeMap); i++ {
		decodeMap[i] = 0xFF
	}
	for i := 0; i < len(s); i++ {
		decodeMap[s[i]] = byte(i)
	}
	return &Alphabet{s, int64(len(s)), decodeMap}
}

type Alphabet struct {
	alphabet  string
	basenum   int64
	decodeMap [256]byte
}

type CorruptInputError int64

func (e CorruptInputError) Error() string {
	return "illegal basex data at input byte " + strconv.FormatInt(int64(e), 10)
}

// Decode a big integer from the bytes. Returns an error on corrupt
// input.
func (a *Alphabet) DecodeToBig(src []byte) (*big.Int, error) {
	n := new(big.Int)
	radix := big.NewInt(a.basenum)
	for i := 0; i < len(src); i++ {
		b := a.decodeMap[src[i]]
		if b == 0xFF {
			return nil, CorruptInputError(i)
		}
		n.Mul(n, radix)
		n.Add(n, big.NewInt(int64(b)))
	}
	return n, nil
}

func (a *Alphabet) Decode(src string) ([]byte, error) {
	n, err := a.DecodeToBig([]byte(src))
	if err != nil {
		return nil, err
	}
	return n.Bytes(), nil
}

func (a *Alphabet) Encode(b []byte) string {
	return string(a.EncodeBig(nil, new(big.Int).SetBytes(b)))
}

// Encode encodes src, appending to dst. Be sure to use the returned
// new value of dst.
func (a *Alphabet) EncodeBig(dst []byte, src *big.Int) []byte {
	start := len(dst)
	n := new(big.Int)
	n.Set(src)
	radix := big.NewInt(a.basenum)
	zero := big.NewInt(0)
	for n.Cmp(zero) > 0 {
		mod := new(big.Int)
		n.DivMod(n, radix, mod)
		dst = append(dst, a.alphabet[mod.Int64()])
	}

	for i, j := start, len(dst)-1; i < j; i, j = i+1, j-1 {
		dst[i], dst[j] = dst[j], dst[i]
	}
	return dst
}
