package basex

import (
	"fmt"
	"math/big"
	"testing"
)

// type testpair struct {
//  	decoded int64
//  	encoded string
// }

var custompairs = []testpair{
	{10002343, "ggf32g1g2"},
	{1000, "fvff"},
	{0, ""},
}

var testCustomAlphabet = "123vfg"

func ExampleEncodeBigCustom() {
	a := NewAlphabet(testCustomAlphabet)
	buf := a.EncodeBig(nil, big.NewInt(123456))
	fmt.Printf("%s\n", buf)
	// Output:
	// 3vg2v31
}

func ExampleDecodeToBigCustom() {
	a := NewAlphabet(testCustomAlphabet)
	n, err := a.DecodeToBig([]byte("3vg2v31"))
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	fmt.Printf("%d\n", n)
	// Output:
	// 123456
}

func TestEncodeCustom(t *testing.T) {
	a := NewAlphabet(testCustomAlphabet)
	for _, p := range custompairs {
		var buf []byte = ([]byte)("noise")
		buf = a.EncodeBig(buf, big.NewInt(p.decoded))
		if string(buf) != "noise"+p.encoded {
			t.Errorf("unexpected result: %q != %q", string(buf), p.encoded)
		}
	}
}

func TestDecodeCustom(t *testing.T) {
	a := NewAlphabet(testCustomAlphabet)
	for _, data := range custompairs {
		var buf []byte = []byte(data.encoded)
		n, err := a.DecodeToBig(buf)
		if err != nil {
			t.Errorf("decoding %q failed: %v", data.encoded, err)
		}
		if n.Int64() != data.decoded {
			t.Errorf("unexpected result: %v != %v", n, data.decoded)
		}
	}
}

func TestDecodeCorruptCustom(t *testing.T) {
	a := NewAlphabet(testCustomAlphabet)
	type corrupt struct {
		input  string
		offset int
	}
	examples := []corrupt{
		{"!!!!", 0},
		{"v===", 1},
		{"v0", 1},
		{"vl", 1},
		{"vI", 1},
		{"vO", 1},
	}

	for _, e := range examples {
		_, err := a.DecodeToBig([]byte(e.input))
		switch err := err.(type) {
		case CorruptInputError:
			if int(err) != e.offset {
				t.Errorf("Corruption in %q at offset %v, want %v", e.input, int(err), e.offset)
			}
		default:
			t.Error("Decoder failed to detect corruption in", e)
		}
	}
}
