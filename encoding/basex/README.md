BaseX encoding for Go
======================

BaseX, where X is the len of your custom alphabet

```
a := basex.NewAlphabet(",./]['-=;'#$(*^&@!`")
s := a.Encode([]byte("hello, world"))
b, _ := a.Decode(s)
fmt.Printf("%s: %q\n", string(b), s)

```

see original base58 package: http://godoc.org/github.com/tv42/base58
