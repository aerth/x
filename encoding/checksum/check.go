package checksum

import (
	"hash/crc32"

	"gitlab.com/aerth/x"
	"gitlab.com/aerth/x/encoding/basex"
	"gitlab.com/aerth/x/encoding/binary"
)

var (
	base58                 = basex.NewAlphabet(basex.Base58Bitcoin)
	crc32q                 = crc32.MakeTable(0xD5828281)
	_      x.StringEncoder = Checker{}
)

// Check returns base58 (btc) encoded crc32 checksum of b
func Check(b []byte) string {
	u := crc32.Checksum(b, crc32q)
	return base58.Encode(binary.MustEncodeL(u))
}

type Checker struct {
	Alphabet *basex.Alphabet
}

func (c Checker) Encode(b []byte) string {
	bin := binary.MustEncodeL(crc32.Checksum(b, crc32q))
	return c.Alphabet.Encode(bin)
}

func (c Checker) EncodeUint32(b []byte) uint32 {
	return crc32.Checksum(b, crc32q)
}

// Decode returns binary encoded uint32
func (c Checker) Decode(s string) ([]byte, error) {
	return c.Alphabet.Decode(s)

}

func (c Checker) DecodeUint(s string) (uint32, error) {
	b, err := c.Decode(s)
	if err != nil {
		return 0, err
	}
	var u uint32
	if err := binary.DecodeL(b, u); err != nil {
		return 0, err
	}
	return u, nil
}
