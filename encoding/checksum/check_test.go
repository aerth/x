package checksum

import "fmt"

func ExampleCheck() {
	str := "hello, world"
	u := Check([]byte(str))
	fmt.Println(u, str)
	str = "hello, world!"
	u = Check([]byte(str))
	fmt.Println(u, str)
	// Output:
	// 5XyuNR hello, world
	// 6Q8axC hello, world!
}
