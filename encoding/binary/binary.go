// Copyright (c) 2018 aerth. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of aerth nor the names of this project's
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package binary

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"

	"gitlab.com/aerth/x"
)

var (
	LittleEndian              = binary.LittleEndian
	BigEndian                 = binary.BigEndian
	_            x.Serializer = &Encoder{}
)

const (
	SizeBool      = 1
	SizeInt8      = 1
	SizeUint8     = 1
	SizeInt16     = 2
	SizeUint16    = 2
	SizeInt32     = 4
	SizeUint32    = 4
	SizeInt64     = 8
	SizeUint64    = 8
	SizeFloat32   = 8
	SizeComplex64 = 8
)

var ErrNotSupported = fmt.Errorf("unsupported type")

func Read(r io.Reader, order binary.ByteOrder, data interface{}) error {
	return binary.Read(r, order, data)
}

func PutUvarint(buf []byte, x uint64) int {
	return binary.PutUvarint(buf, x)
}

func PutVarint(buf []byte, x int64) int {
	return binary.PutVarint(buf, x)
}

// EncodeB tries to return the BigEndian binary representation of v
func EncodeB(v interface{}) ([]byte, error) {
	return encode(BigEndian, v)
}

// EncodeL tries to return the LittleEndian binary representation of v
func EncodeL(v interface{}) ([]byte, error) {
	return encode(LittleEndian, v)
}

func MustEncodeB(v interface{}) []byte {
	b, err := encode(BigEndian, v)
	if err != nil {
		panic(err)
	}
	return b
}
func MustEncodeL(v interface{}) []byte {
	b, err := encode(LittleEndian, v)
	if err != nil {
		panic(err)
	}
	return b
}

func encode(order binary.ByteOrder, v interface{}) ([]byte, error) {
	buf := bytes.Buffer{}
	err := binary.Write(&buf, order, v)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func DecodeB(input []byte, output interface{}) error {
	return decode(BigEndian, bytes.NewReader(input), output)
}

func DecodeL(input []byte, output interface{}) error {
	return decode(LittleEndian, bytes.NewReader(input), output)
}

func decode(order binary.ByteOrder, input io.Reader, output interface{}) error {
	return binary.Read(input, order, output)
}

type Encoder struct {
	ByteOrder binary.ByteOrder
}

func (e *Encoder) MustSerialize(v interface{}) []byte {
	b, err := encode(e.ByteOrder, v)
	if err != nil {
		panic(err)
	}
	return b
}

func (e *Encoder) Serialize(v interface{}) ([]byte, error) {
	return encode(e.ByteOrder, v)
}

func (e *Encoder) Deserialize(b []byte, v interface{}) error {
	return decode(e.ByteOrder, bytes.NewReader(b), v)
}
