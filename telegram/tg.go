package telegram

import (
	"fmt"
	"io"
	"net/http"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var testbot = ""

type Bot struct {
	tg   *tgbotapi.BotAPI
	data map[string]interface{}
	defs map[string]string
}

func New(token string, httpclient *http.Client) (*Bot, error) {
	bot, err := tgbotapi.NewBotAPIWithClient(token, httpclient)
	if err != nil {
		return nil, err
	}
	return &Bot{tg: bot, data: map[string]interface{}{}, defs: map[string]string{}}, nil
}
func (b *Bot) Bot() *tgbotapi.BotAPI {
	return b.tg
}

func DefaultUpdateConfig() tgbotapi.UpdateConfig {
	return tgbotapi.UpdateConfig{
		Timeout: 30,
	}
}

var NewUpdate = tgbotapi.NewUpdate
var NewMessage = tgbotapi.NewMessage

func (b *Bot) Send(msg tgbotapi.MessageConfig) (tgbotapi.Message, error) {
	return b.tg.Send(msg)
}

func (b *Bot) UpdateChan() (tgbotapi.UpdatesChannel, error) {
	return b.tg.GetUpdatesChan(DefaultUpdateConfig())
}

func LogMessage(w io.Writer, msg *tgbotapi.Message) {
	date := time.Unix(int64(msg.Date), 0)
	fmt.Fprintf(w, "[%s][%s][%s] %s\n", date.Format(time.Kitchen), msg.Chat.Title, msg.From.String(), msg.Text)
}
