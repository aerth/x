package main

import (
	"fmt"
	"log"
	"os"

	"github.com/aerth/tgun"
	"github.com/kr/pretty"

	"gitlab.com/aerth/x/telegram"
)

func main() {
	tgn := &tgun.Client{
		//		Proxy: "tor",
		Proxy: "socks5://127.0.0.1:1080",
	}
	httpclient, err := tgn.HTTPClient()
	if err != nil {
		log.Fatalln(err)
	}
	bot, err := telegram.New("", httpclient)
	if err != nil {
		log.Fatalln(err)
	}

	tg := bot.Bot()
	tg.Debug = false
	log.Printf("Bot authenticated as %s", tg.Self.UserName)
	updates, err := bot.UpdateChan()
	if err != nil {
		log.Fatalln(err)
	}
	for update := range updates {
		if update.Message == nil {
			pretty.Println(update)
			continue
		}
		fmt.Println()
		telegram.LogMessage(os.Stderr, update.Message)

		if false {
			msg := telegram.NewMessage(update.Message.Chat.ID, update.Message.Text)
			msg.ReplyToMessageID = update.Message.MessageID
			go func() {
				if _, err := bot.Send(msg); err != nil {
					log.Fatalln("ERROR SENDING:", err)
				}
			}()
		}
	}
}
