package argon2id

import (
	"bytes"
	"hash"

	a2 "golang.org/x/crypto/argon2"
)

var _ hash.Hash = &Argon2id{}

var HashSize uint32 = 32

var Salt []byte

type Argon2id struct {
	x   uint32
	y   uint32
	z   uint8
	buf bytes.Buffer
}

func NewDefault() *Argon2id {
	return New(3, 512*1024, 4) // 3x, 512 MB, 4thread
}

func New(x, y uint32, z uint8) *Argon2id {
	return &Argon2id{
		x:   x,
		y:   y,
		z:   z,
		buf: bytes.Buffer{},
	}
}

func (a *Argon2id) BlockSize() int {
	return 64 // definitely wrong
}

func (a *Argon2id) Reset() {
	a.buf.Reset()
}
func (a *Argon2id) Sum(b []byte) []byte {
	a.buf.Write(b)
	total := a.buf.Bytes()
	a.buf.Reset()
	return a2.IDKey(total, Salt, a.x, a.y, a.z, HashSize)
}
func (a *Argon2id) Size() int { return int(HashSize) }
func (a *Argon2id) Write(b []byte) (n int, err error) {
	return a.buf.Write(b)
}
