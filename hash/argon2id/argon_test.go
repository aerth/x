package argon2id

import (
	"fmt"
	"testing"
)

func TestOne(t *testing.T) {
	argon := NewDefault()
	b := argon.Sum(helloworld)
	fmt.Printf("%x\n", b)
	b = argon.Sum(helloworld)
	fmt.Printf("%x\n", b)
}

var helloworld = []byte("hello world")

func BenchmarkSafeArgon(b *testing.B) {
	argon := NewDefault()
	for i := 0; i < b.N; i++ {
		argon.Sum(helloworld)
	}
}

func BenchmarkQuickArgon(b *testing.B) {
	argon := New(1, 1, 1)

	for i := 0; i < b.N; i++ {
		argon.Sum(helloworld)
	}
}

func BenchmarkSlowArgon(b *testing.B) {
	argon := New(1000, 1, 1)

	for i := 0; i < b.N; i++ {
		argon.Sum(helloworld)
	}
}
