package database

import (
	"fmt"
	"time"

	"gitlab.com/aerth/x/encoding/rlp"
	"gitlab.com/aerth/x/logger"

	"github.com/boltdb/bolt"
)

var log = logger.New(nil, "[DB]", LogLevel)

var LogLevel = 0

var ErrNotFound = fmt.Errorf("not found")

type DB struct {
	db *bolt.DB
}

func (d *DB) Close() error {
	return d.db.Close()
}
func OpenConfig(filename string, bucket []byte, options *bolt.Options) (*DB, error) {
	log.Tracef("opening db: %q [%x]", filename, bucket)
	db, err := bolt.Open(filename, 0666, options)
	if err != nil {
		return nil, err
	}
	if bucket != nil {

		if err := db.Update(func(tx *bolt.Tx) error {
			_, err = tx.CreateBucketIfNotExists(bucket)
			return err
		}); err != nil {
			return nil, err
		}

	}
	return &DB{db: db}, nil
}

func Open(filename string, buckets ...[]byte) (*DB, error) {
	log.Tracef("opening db: %q [%v buckets]", filename, len(buckets))
	db, err := bolt.Open(filename, 0666, &bolt.Options{ReadOnly: false, Timeout: 3 * time.Second})
	if err != nil {
		return nil, err
	}
	if len(buckets) > 0 {
		for _, bucket := range buckets {
			if err := db.Update(func(tx *bolt.Tx) error {
				_, err = tx.CreateBucketIfNotExists(bucket)
				return err
			}); err != nil {
				return nil, err
			}
		}
	}
	return &DB{db: db}, nil
}

func (d *DB) ReadNested(bucket []byte, key []byte, field []byte) []byte {
	var encoded []byte
	d.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket)
		if b == nil {
			return nil
		}
		b2 := b.Bucket(key)
		if b2 == nil {
			return nil
		}
		v := b2.Get(field)
		encoded = v
		return nil
	})
	return encoded
}

func (d *DB) Read(bucket []byte, key []byte) []byte {
	var encoded []byte
	d.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket)
		v := b.Get(key)
		encoded = v
		return nil
	})
	return encoded
}

func (d *DB) ReadRLP(bucket []byte, key []byte, decoded interface{}) error {
	encoded := d.Read(bucket, key)
	if len(encoded) == 0 {
		return ErrNotFound
	}
	log.Tracef("rlp decode: %x %x %x", bucket, key, encoded)
	return rlp.DecodeBytes(encoded, &decoded)
}

func (d *DB) WriteRLP(bucket []byte, key []byte, thing interface{}) error {
	encoded, err := rlp.EncodeToBytes(thing)
	if err != nil {
		return err
	}
	return d.Write(bucket, key, encoded)

}

func (d *DB) WriteNested(bucket []byte, key []byte, field []byte, encoded []byte) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(bucket)
		if err != nil {
			return err
		}
		kb, err := b.CreateBucketIfNotExists(key)
		if err != nil {
			return err
		}
		err = kb.Put(field, encoded)
		return err
	})
}
func (d *DB) Write(bucket []byte, key []byte, encoded []byte) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(bucket)
		if err != nil {
			return err
		}
		return b.Put(key, encoded)
	})
}

func (d *DB) Bolt() *bolt.DB {
	return d.db
}
