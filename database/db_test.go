package database

import (
	"bytes"
	"fmt"
	"os"
	"testing"
)

func TestOne(t *testing.T) {
	t.Log("creating dummy.db")
	db, err := Open("dummy.db")

	if err != nil {
		t.Error(err)
	}

	defer func() {
		t.Log("removing dummy.db")
		os.Remove("dummy.db")
	}()

	a, b, c, d := []byte("a"), []byte("b"), []byte("c"), []byte("d")

	if err := db.WriteNested(a, b, c, d); err != nil {
		t.Error(err)
	}

	x := db.ReadNested(a, b, c)
	if bytes.Compare(d, x) != 0 {
		t.Log("unequal:")
		fmt.Printf("d=%x\nx=%x\n", d, x)
		t.FailNow()
	}
}
