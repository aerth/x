module gitlab.com/aerth/x

go 1.14

require (
	github.com/aerth/tgun v0.1.4
	github.com/boltdb/bolt v1.3.1
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/cmars/basen v0.0.0-20150613233007-fe3947df716e
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/emersion/go-sasl v0.0.0-20191210011802-430746ea8b9b
	github.com/emersion/go-smtp v0.12.1
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/kr/pretty v0.2.0
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.6
	github.com/mattn/go-isatty v0.0.12
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/tv42/base58 v0.0.0-20150113235651-b6649477bfe6
	golang.org/x/crypto v0.0.0-20200403201458-baeed622b8d8
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200331124033-c3d80250170d // indirect
	launchpad.net/gocheck v0.0.0-20140225173054-000000000087 // indirect
)
