// +build ignore

package main

import (
	"os"
	"os/exec"
	"path/filepath"
)

func main() {

	// go install -tags 'osusergo netgo' -ldflags '-w -s -extldflags -static' ./...
	cmd := exec.Command("go", "install", "-tags", "osusergo netgo static", "-ldflags", "-w -s -extldflags -static", "./...")
	pwd, err := os.Getwd()
	if err != nil {
		println(err.Error())
		os.Exit(111)
	}
	gopath := os.Getenv("GOPATH")
	gobin := filepath.Join(pwd, "bin")

	cmd.Env = append(cmd.Env, "HOME="+os.Getenv("HOME"))
	cmd.Env = append(cmd.Env, "GOBIN="+gobin)
	cmd.Env = append(cmd.Env, "HOME="+os.Getenv("HOME"))
	cmd.Env = append(cmd.Env, "PATH="+os.Getenv("PATH"))
	cmd.Env = append(cmd.Env, "GOPATH="+gopath)
	println("gopath:", gopath)
	println("building to:", gobin)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		println(err.Error())
		os.Exit(111)
	}
}
