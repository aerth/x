// Copyright (c) 2018 aerth. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of aerth nor the names of this project's
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// package market contains market related utilities, interfaces, and constants
package market

import (
	"fmt"
	"math/big"
	"time"
)

// Pair is a string, such as "AQUA/BTC". Exchanges may have
// different string which is a market id that is not part of
// this package. A pair should be human readable.
type Pair string

// Exchange client provides the basics.
// Each should return an error, if any, that describes why the
// results could not be fetched.
type Exchange interface {
	// Tickers returns all Tickers available
	Tickers() ([]Ticker, error)

	// Ticker returns ticker for Pair
	Ticker(Pair) (Ticker, error)

	// Markets returns Pairs, and the ID the exchange uses
	Markets() ([]Pair, map[Pair]string, error)

	// OrderBook returns all asks and bids (ExecutedTrades) for a Pair
	OrderBook(Pair) (Orderbook, error)

	// String returns a human readable name of the exchange
	String() string
}

// Ticker in timestamped envelope
type Ticker struct {
	At     int `json:"at"`
	Ticker struct {
		Buy  string `json:"buy"`
		Sell string `json:"sell"`
		Low  string `json:"low"`
		High string `json:"high"`
		Last string `json:"last"`
		Vol  string `json:"vol"`
	} `json:"ticker"`
}

// Order is an uncompleted trade (can be partially executed)
type Order struct {
	ID              int        `json:"id"`
	Side            string     `json:"side"`
	OrdType         string     `json:"ord_type"`
	Price           *big.Float `json:"price"`
	AvgPrice        string     `json:"avg_price"`
	State           string     `json:"state"`
	Market          Pair       `json:"market"`
	CreatedAt       time.Time  `json:"created_at"`
	Volume          *big.Float `json:"volume"`
	RemainingVolume *big.Float `json:"remaining_volume"`
	ExecutedVolume  *big.Float `json:"executed_volume"`
	TradesCount     int        `json:"trades_count"`
}

func (o Order) String() string {
	return fmt.Sprintf("%v/%s [%4s] (%s) %0.8f [%0000.4f]",
		o.ID, o.Market, o.Side, o.OrdType, o.Price, o.Volume)
}

// Orderbook
type Orderbook struct {
	Asks []Order `json:"asks"`
	Bids []Order `json:"bids"`
}

type MarketResult struct {
	ID   string `json:'id'`
	Name string `json:'name'`
}
