package stringz

func Trim(s string) string {
	if len(s) > 10 {
		return s[:10]
	}
	return s
}
