package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"

	"github.com/aerth/tgun"
)

func main() {
	t := tgun.Client{}
	if len(os.Args) == 1 {
		fmt.Println("enter URL to download and format")
		os.Exit(111)
	}
	b, err := t.GetBytes(os.Args[1])
	if err != nil {
		println(err.Error())
		os.Exit(111)
	}
	buf := new(bytes.Buffer)
	for i := range b {
		fmt.Fprintf(buf, "0x%x, ", b[i])
	}
	out := strings.TrimSuffix(buf.String(), ", ")
	fmt.Println(out)
}
