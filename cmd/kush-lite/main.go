package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"

	"gitlab.com/aerth/x/mirror/shlex"
)

var promptWithSpace = []byte(os.ExpandEnv("*> "))

const usage = `` +
	`
kush shell (https://gitlab.com/aerth/x/)

`

func main() {
	runtime.LockOSThread()
	var (
		debug = flag.Bool("v", false, "echo parsed input")
		noop  = flag.Bool("dry", false, "dont execute anything")
	)
	flag.Parse()
	s := bufio.NewScanner(os.Stdin)
	os.Stdout.Write(promptWithSpace)
	for s.Scan() {
		// put in func so we can defer prompt
		func() {
			defer os.Stdout.Write(promptWithSpace)
			cmdline, err := shlex.Split(s.Text())
			if err != nil {
				println(err.Error())
				return
			}

			// expand env
			for i := range cmdline {
				cmdline[i] = os.ExpandEnv(cmdline[i])
			}

			// echo parsed
			if *debug {
				fmt.Println(cmdline)
			}

			// dry-run
			if *noop {
				return
			}

			// built-ins (cd)
			if isBuiltIn(cmdline) {
				return
			}

			// execute command
			var cmd *exec.Cmd
			var runcmd func() error
			if len(cmdline) == 1 {
				cmd = exec.Command(cmdline[0])
			} else {
				cmd = exec.Command(cmdline[0], cmdline[1:]...)
			}

			runcmd = cmd.Run
			// runcmd = cmd.Start // '&'

			cmd.Stdout = os.Stdout
			cmd.Stdin = os.Stdin
			cmd.Stderr = os.Stderr

			if err := runcmd(); err != nil {
				println(err.Error())
				return
			}

		}()
	}
}

func isBuiltIn(cmdline []string) (handled bool) {
	if len(cmdline) == 0 {
		return true
	}
	if strings.Contains(cmdline[0], "\x1b") {
		return true
	}
	switch cmdline[0] {
	case "cd":
		if len(cmdline) > 2 {
			println("'cd' requires one argument")
		}
		if len(cmdline) == 1 {
			cmdline = append(cmdline, os.ExpandEnv("$HOME"))
		}
		if err := os.Chdir(cmdline[1]); err != nil {
			println(err.Error())
		}
	case "help":
		println(usage)
	default:
		return false
	}

	return true
}
