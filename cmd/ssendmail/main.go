package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"

	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
)

func defaultConfigLocation() string {
	return "ssend.conf"
}

type config struct {
	Serverport string `json: 'serverport'` // 1.2.3.4:25
	From       string `json: 'from'`
	Pass       string `json: 'pass'`

	To          []string      `json: 'to'`
	Subject     string        `json: 'subject'`
	Body        string        `json: 'body'`
	Attachments []interface{} `json: 'attachments'`
}

func readfile(name string) string {
	b, err := ioutil.ReadFile(name)
	if err != nil {
		fmt.Println("error reading input:", err)
		os.Exit(111)
	}
	return string(b)
}

func main() {
	var config config
	var configloc = defaultConfigLocation()

	data, _ := ioutil.ReadFile(configloc)
	if err := json.Unmarshal(data, &config); err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	fmt.Println("Sending from:", config.From)

	if len(os.Args) < 2 {
		fmt.Println("Config file found, but no mail to send.")
		os.Exit(111)
	}

	if len(os.Args) > 3 {
		fmt.Println("Too many arguments")
		os.Exit(111)
	}

	if len(os.Args) == 2 && len(config.To) == 0 {
		fmt.Println("No destination, see usage")
		os.Exit(111)
	} else if len(os.Args) == 2 && len(config.To) != 0 {
		fmt.Println("Reading message body from config")
		config.Body = readfile(os.Args[1])
	}

	if len(os.Args) == 3 {
		config.To = strings.Split(os.Args[1], ",")
		config.Body = readfile(os.Args[2])
	}

	if (len(config.To) == 0) || false {
		fmt.Println("invalid config")
		os.Exit(111)
	}
	message := ""
	headers := map[string]string{}
	headers["From"] = config.From
	headers["To"] = config.To[0]
	headers["Subject"] = config.Subject

	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + config.Body

	auth := sasl.NewPlainClient("", config.From, config.Pass)
	host, _, _ := net.SplitHostPort(config.Serverport)
	tlsconfig := &tls.Config{
		ServerName: host,
	}
	conn, err := tls.Dial("tcp", config.Serverport, tlsconfig)
	if err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	client, err := smtp.NewClient(conn, host)
	if err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	if err := client.Auth(auth); err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	if err := client.Mail(config.From, &smtp.MailOptions{}); err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	if err := client.Rcpt(config.To[0]); err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	w, err := client.Data()
	if err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	if err = w.Close(); err != nil {
		fmt.Println("fatal:", err)
		os.Exit(111)
	}

	client.Quit()

}

func decodepass(in string) string {
	return string(in)
}
