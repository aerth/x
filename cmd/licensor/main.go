package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func main() {

	var (
		description = "licensor adds a license to go files\n"
		usage       = description + "usage:\n\tlicensor license.go <go-files...>\n"
		licenseFile = os.ExpandEnv("$HOME/go/src/LICENSE.go") // default loc
	)

	if len(os.Args) == 1 {
		fmt.Println(usage)
		os.Exit(111)
	}

	if gopath := os.Getenv("GOPATH"); gopath != "" {
		licenseFile = filepath.Join(gopath, "src", "LICENSE.go")
	}

	var (
		licenseFlag = flag.String("l", "", "path to license, default: "+licenseFile)
	)

	flag.Parse()

	if *licenseFlag != "" {
		licenseFile = *licenseFlag
	}

	if len(flag.Args()) == 0 {
		fmt.Println(usage)
		os.Exit(111)
	}

	b, err := ioutil.ReadFile(licenseFile)
	if err != nil {
		log.Println(err)
		os.Exit(111)
	}
	args := flag.Args()
	var wins, fails int
	for i := range args {
		original, ok := handleRead(args[i])
		if !ok {
			continue
		}
		if !handleWrite(b, original, args[i]) {
			fails++
		}
		wins++
	}
	println("finished with", fails, "fails,", wins, "wins")
}
func handleRead(filename string) (original []byte, ok bool) {
	println("reading", filename)
	f, err := os.Open(filename)
	if err != nil {
		log.Println(err)
		return nil, false
	}
	defer f.Close()
	if !peakFile(f) {
		return nil, false
	}

	log.Println("MATCH:", filename)

	// read entire file
	if n, err := f.Seek(0, 0); err != nil || n != 0 {
		log.Println("READ: could not seek", err)
		return nil, false
	}
	original, err = ioutil.ReadAll(f)
	if err != nil {
		log.Println("READ:", err)
		return nil, false
	}
	return original, true
}
func handleWrite(licenseBytes, original []byte, filename string) bool {
	// stat for mode
	println("writing", filename)
	f, _ := os.Open(filename)
	stat, err := f.Stat()
	f.Close()
	if err != nil {
		log.Println("STAT:", err)
		return false
	}

	// reopen write
	f, err = os.OpenFile(filename, os.O_RDWR, stat.Mode())
	if err != nil {
		log.Println("OPEN WRITE:", err)
		return false
	}

	defer f.Close()

	// overwrite with license blob
	_, err = f.WriteAt(licenseBytes, 0)
	if err != nil {
		log.Println("WRITE:", err)
		return false
	}

	// overwrite with original
	_, err = f.WriteAt(original, int64(len(licenseBytes)))
	if err != nil {
		log.Println("WRITE:", err)
		return false
	}

	return true

}

func peakFile(r io.Reader) bool {
	b := make([]byte, 8, 8)
	if _, err := r.Read(b); err != nil {
		log.Println("PEAK:", err)
		return false
	}
	if bytes.Compare(b, []byte{0x70, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65, 0x20}) == 0 {
		return true
	}
	return false
}
